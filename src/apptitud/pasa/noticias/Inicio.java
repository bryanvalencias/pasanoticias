package apptitud.pasa.noticias;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;



import com.google.android.gcm.GCMRegistrar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("SetJavaScriptEnabled")
public class Inicio extends Activity {

	ProgressDialog progressBar;
	// private int progressBarStatus = 0;
	// private Handler progressBarHandler = new Handler();
	private WebView wv_pagina;
	public String name;
	public String pass;
	public static final String producto = "pasanoticia";
	public Activity activity = this;
	private final static String IdRegistr = "891672553565";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inicio);

		this.setTitle(this.getString(R.string.TituloActividad));
		wv_pagina = (WebView) findViewById(R.id.wv_pagina);

		GCMRegistrar.checkDevice(Inicio.this);
		GCMRegistrar.checkManifest(Inicio.this);

		wv_pagina.setWebViewClient(new MiWebViewClient());
		wv_pagina.setWebChromeClient(new MiChromeWebViewClient());
		wv_pagina.getSettings().setJavaScriptEnabled(true);
		wv_pagina.addJavascriptInterface(new MiWebInterface(this), "Android");
		wv_pagina.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);

		CookieSyncManager.createInstance(this);
		final handlerDB dataBase = new handlerDB(activity);
		dataBase.abrir();
		if (dataBase.leer("name") != null || dataBase.leer("pass") != null) {

			String url = "http://circobits.com/movil1/splash_screen.php?id="
					+ dataBase.leer("id") + "&producto=" + producto;
			wv_pagina.loadUrl(url);

		} else {
			AlertDialog.Builder mensajeEmergente = new AlertDialog.Builder(
					activity);
			mensajeEmergente.setTitle(R.string.TtlMsjAlerta);
			mensajeEmergente.setMessage(R.string.msjSinIniciarSesion);
			mensajeEmergente.setPositiveButton(R.string.btnSi,
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							iniciarSesion();

						}
					});

			mensajeEmergente.setNegativeButton(R.string.btnNo,
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							activity.finish();

						}
					});

			mensajeEmergente.create();
			mensajeEmergente.show();
		}
		dataBase.cerrar();
	}

	@Override
	protected void onStop() {
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.removeAllCookie();
		wv_pagina.clearHistory();
		wv_pagina.clearCache(true);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.removeAllCookie();
		wv_pagina.clearHistory();
		wv_pagina.clearCache(true);
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_inicio, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		AlertDialog.Builder mensajeEmergente = new AlertDialog.Builder(activity);
		final handlerDB dataBase = new handlerDB(activity);
		dataBase.abrir();
		switch (item.getItemId()) {

		case R.id.IniciarSesion2:

			Log.w("Iniciar sesion", "IniciarSesion2");
			if (dataBase.leer("name") != null || dataBase.leer("pass") != null) {

				mensajeEmergente.setTitle(R.string.TtlMsjAlerta);
				mensajeEmergente.setMessage(R.string.msjSinCerrarSesion);
				mensajeEmergente.setPositiveButton(R.string.btnAceptar, null);
				mensajeEmergente.create();
				mensajeEmergente.show();

			} else {
				this.iniciarSesion();
			}

			dataBase.cerrar();
			break;
		case R.id.CerrarSesion1:

			if (dataBase.leer("name") != null || dataBase.leer("pass") != null) {

				final String regId = GCMRegistrar.getRegistrationId(this);
				mensajeEmergente.setTitle(R.string.TtlMsjAlerta);
				mensajeEmergente.setMessage(R.string.msjCerrarSesion);
				mensajeEmergente.setPositiveButton(R.string.btnSi,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								dataBase.eliminar();
								dataBase.cerrar();
								if (!regId.equals("")) {
									GCMRegistrar.unregister(activity);
								} else {
									Log.v("GCMTest", "Ya des-registrado");
								}
								activity.finish();

							}
						});

				mensajeEmergente.setNegativeButton(R.string.btnNo, null);

				mensajeEmergente.create();
				mensajeEmergente.show();

			} else {
				mensajeEmergente.setTitle(R.string.TtlMsjAlerta);
				mensajeEmergente.setMessage(R.string.msjNoHaySesionIniciada);
				mensajeEmergente.setPositiveButton(R.string.btnAceptar, null);
				mensajeEmergente.create();
				mensajeEmergente.show();
			}

			Log.w("Cerrar sesion", "CerrarSesion1");

			dataBase.cerrar();
			break;
		default:
			return super.onOptionsItemSelected(item);

		}
		dataBase.cerrar();
		return false;

	}

	/* Construye la URL */
	public String Dir(String name, String pass) {
		String url = "http://www.circobits.com/movil1/PrEmIuNcIRCo.php?Usuario="
				+ name + "&Noticia=" + pass + "&Producto=" + producto;

		return url;

	}

	private void EnviarDatos(String url) {

		HttpClient httpclient = new DefaultHttpClient();

		HttpPost HttpPost = new HttpPost(url);
		try {

			HttpResponse response = httpclient.execute(HttpPost);
			String jsonResult = inputStreamToString(
					response.getEntity().getContent()).toString();
			JSONObject obj = new JSONObject(jsonResult);
			Log.e("datos", response.toString());

			if (obj.getString("estado").equals("true")) {
				final String id = obj.getString("id");

				runOnUiThread(new Runnable() {
					public void run() {
						handlerDB dataBase = new handlerDB(activity);
						dataBase.abrir();
						if (dataBase.leer("name") == null
								|| dataBase.leer("pass") == null) {
							dataBase.registrar(name, pass, id);
						} else {
							int result = dataBase.eliminar();
							if (result >= 0) {
								dataBase.registrar(name, pass, id);
							}
						}

						if (dataBase.leer("name") != null
								|| dataBase.leer("pass") != null
								|| dataBase.leer("id") != null) {

							String url = "http://circobits.com/movil1/splash_screen.php?id="
									+ id + "&producto=" + producto;
							wv_pagina.loadUrl(url);
						}
						dataBase.cerrar();
						final String regId = GCMRegistrar
								.getRegistrationId(activity);

						if (regId.equals("")) {
							GCMRegistrar.register(activity, IdRegistr); // Sender
																		// ID
						} else {
							Log.v("Inicio_Pasanoticias", "Ya registrado");
						}

					}
				});
			} else {
				runOnUiThread(new Runnable() {
					public void run() {
						AlertDialog.Builder mensajeEmergente = new AlertDialog.Builder(
								activity);
						mensajeEmergente
								.setTitle(R.string.TtlMsjDatosIncorretos);
						mensajeEmergente
								.setMessage(R.string.msjDatosIncorretos);
						mensajeEmergente.setNeutralButton(R.string.btnAceptar,
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int which) {
										iniciarSesion();

									}
								});

						mensajeEmergente.create();
						mensajeEmergente.show();

					}
				});
			}

		} catch (IOException e) {
			Log.e("Error", e.fillInStackTrace().toString());
		} catch (JSONException e) {

			Log.e("Error", e.fillInStackTrace().toString());

		}
	}

	private StringBuilder inputStreamToString(InputStream is) {
		String rLine = "";
		StringBuilder answer = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		try {
			while ((rLine = rd.readLine()) != null) {
				answer.append(rLine);
			}
		}

		catch (IOException e) {
			e.printStackTrace();
		}
		return answer;
	}

	@SuppressLint("InflateParams")
	public void iniciarSesion() {
		AlertDialog.Builder mensajeEmergente = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		final View Inflator = inflater.inflate(R.layout.inicio_sesion, null);
		final EditText ed1 = (EditText) Inflator.findViewById(R.id.etName);
		final EditText ed2 = (EditText) Inflator.findViewById(R.id.etPass);

		mensajeEmergente.setTitle(R.string.menu_Iniciar);
		mensajeEmergente.setView(Inflator);
		mensajeEmergente.setPositiveButton(R.string.btnIniciar,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// hilo grafico carga la URL
						new Thread(new Runnable() {

							@Override
							public void run() {

								name = ed1.getText().toString();
								pass = ed2.getText().toString();

								EnviarDatos(Dir(ed1.getText().toString(), ed2
										.getText().toString()));

							}
						}).start();

					}
				});
		mensajeEmergente.setNegativeButton(R.string.btnCancelar,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						activity.finish();

					}
				});

		mensajeEmergente.create();
		mensajeEmergente.show();
	}

	private class MiWebViewClient extends WebViewClient {

		private void iniciaCarga(WebView v) {
			if (progressBar != null) {
				// pbCancel();
			}
			// progressBar = new ProgressDialog(v.getContext());
			// progressBar.setTitle(R.string.TituloProgressBar);
			// progressBar.setMessage(getString(R.string.msjProgressBar));
			// progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			// progressBar.setCancelable(false);
			// pbShow();
			// progressBarStatus = 0;
			// runOnUiThread(new Runnable() {
			//
			// @Override
			// public void run() {
			// progressBarHandler.post(new Runnable() {
			// public void run() {
			// try {
			// progressBar.setProgress(progressBarStatus++);
			// } catch (Exception e) {
			//
			// }
			// }
			// });
			// }
			// });
		}

		// private void pbCancel() {
		// try {
		// progressBar.cancel();
		// } catch (Exception e) {
		// Log.e("ERROR", e.getMessage());
		// }
		// }

		// public void pbShow() {
		// try {
		// progressBar.show();
		// } catch (Exception e) {
		// Log.e("ERROR", e.getMessage());
		// }
		// }

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			Log.i("CARGANDO_PAGINA", url);
			iniciaCarga(view);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);

		}
	}

	private class MiChromeWebViewClient extends WebChromeClient {

	}

	private class MiWebInterface {
		Context contexto;

		/** Instantiate the interface and set the context */
		MiWebInterface(Context c) {
			contexto = c;
		}

		/** Show a toast from the web page */
		@JavascriptInterface
		public void mostrarMensaje(String toast) {
			Toast.makeText(contexto, toast, Toast.LENGTH_SHORT).show();
		}

	}

}
