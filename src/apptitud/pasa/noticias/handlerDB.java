package apptitud.pasa.noticias;

import static android.provider.BaseColumns._ID;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class handlerDB extends SQLiteOpenHelper {
	static String nombreBaseDatos = "pasanoticias_circobits"; // db name

	// tablas
	static String nombreTabla1 = "usuario";

	// columnas
	static String columName = "name";
	static String columPass = "pass";
	static String columId = "id";

	// Sentencia SQL para crear la tabla 1
	public String queryCreateTable1 = "CREATE TABLE " + nombreTabla1 + " ("
			+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + columName
			+ " TEXT, " + columPass + " TEXT, " + columId + " TEXT)";
	public String queryDropTable1 = "DROP TABLE IF EXISTS " + nombreTabla1;

	public int numTuplasTb1;

	public handlerDB(Context context) {
		super(context, nombreBaseDatos, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(queryCreateTable1);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(queryDropTable1);
		this.onCreate(db);

	}

	/* metodo que ingresa los datos a la tabla 1 */
	public void registrar(String name, String pass, String id) {

		ContentValues valores = new ContentValues();
		valores.put("name", name);
		valores.put("pass", pass);
		valores.put("id", id);

		this.getWritableDatabase().insert(nombreTabla1, null, valores);

	}

	/* metodo que devuelve el valor solicitado de la tabla1 */
	public String leer(String columna) {
		String result = null;
		String columnas[] = { "_ID", columName, columPass, columId };
		Cursor c = this.getReadableDatabase().query(nombreTabla1, columnas,
				null, null, null, null, null);
		int name, pass, id;

		name = c.getColumnIndex(columName);
		pass = c.getColumnIndex(columPass);
		id = c.getColumnIndex(columId);

		numTuplasTb1 = c.getCount();
		c.moveToLast();

		if (numTuplasTb1 != 0) {
			if (columna.equals(columName)) {
				result = c.getString(name);
				return result;
			} else {
				if (columna.equals(columPass)) {
					result = c.getString(pass);
					return result;
				} else {
					if (columna.equals(columId)) {
						result = c.getString(id);
						return result;
					} else {
						return result;
					}
				}
			}
		} else {
			return result;
		}
	}

	/* elimina la fila de tabla 1 */
	public int eliminar() {

		return this.getWritableDatabase().delete(nombreTabla1, null, null);

	}

	/* abrir */
	public void abrir() {
		this.getWritableDatabase();
	}

	/* cerrar */
	public void cerrar() {
		this.close();

	}
}
