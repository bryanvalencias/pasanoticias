package apptitud.pasa.noticias;
import java.util.ArrayList;

import android.provider.Settings.Secure;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;



import com.google.android.gcm.GCMBaseIntentService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;



public class GCMIntentService extends GCMBaseIntentService {

	
	private final static String IdRegistr = "891672553565";
	public final static String url = "http://circobits.com/movil1/clientes/notificaciones/index.php";

	/* Constructor */
	public GCMIntentService() {
		super(IdRegistr);
	}

	@Override
	protected void onError(Context context, String errorId) {
		Log.e("GCMIntentService_PasaNoticias", "REGISTRATION: Error -> " + errorId);

	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		String msg = intent.getExtras().getString("message");
		Log.d("GCMIntentService_PasaNoticias", "Mensaje: " + msg);
		this.notificarMensaje(context, msg);

	}

	@Override
	protected void onRegistered(Context context, String regId) {
		Log.i("GCMIntentService_PasaNoticias", regId);
		handlerDB db = new handlerDB(context);
		db.abrir();
		String usuario = db.leer("name");
		db.cerrar();
		this.registroServidor(usuario, regId);

	}

	private void registroServidor(String usuario, String regId) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		int i = 0;
		try {

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("tag", "usersave"));
			nameValuePairs.add(new BasicNameValuePair("username", usuario));
			nameValuePairs.add(new BasicNameValuePair("gcmcode", regId));
			nameValuePairs.add(new BasicNameValuePair("producto", "boletin"));
			nameValuePairs.add(new BasicNameValuePair("idphone", Secure
					.getString(getApplicationContext().getContentResolver(),
							Secure.ANDROID_ID)));
			

			if (nameValuePairs != null)
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			Log.e("GCM", "NAMEVALUEPAIRS: " + nameValuePairs);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String res = httpclient.execute(httppost, responseHandler);

			Log.w("GCM", "RES: " + res);
			i = 1;

		} catch (Exception e) {
			i = 2;

			Log.d("GCMIntentService_PasaNoticias",
					"Error al envio del registro al servidor WEb"
							+ e.getCause() + " /// " + e.getMessage());
		} finally {

			if (i == 1) {
				Log.i("GCMIntentService_PasaNoticias",
						"Finalizo el try correctamente");
			} else {
				if (i == 2) {
					Log.d("GCMIntentService_PasaNoticias",
							"No Finalizo el try y entro al catch");
				}
			}

		}

	}

	@SuppressWarnings("deprecation")
	private void notificarMensaje(Context context, String msg) {
		String notificationService = Context.NOTIFICATION_SERVICE;
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(notificationService);

		// Configuramos la notificación
		int icono = R.drawable.ic_launcher;
		CharSequence estado = this.getString(R.string.NotificaPendiente);
		long hora = System.currentTimeMillis();

		Notification notification = new Notification(icono, estado, hora);
		long[] vibrate = { 100, 100, 200, 300 };
		notification.vibrate = vibrate;

		// Configuramos el Intent
		Context contexto = context.getApplicationContext();
		CharSequence titulo = getString(R.string.TituloNotificacion);
		CharSequence descripcion = msg;

		Intent intent = new Intent(contexto, Inicio.class);
		Bundle b = new Bundle();
		b.putInt("update", 1);
		intent.putExtra("android.intent.extra.INTENT", b);

		PendingIntent contIntent = PendingIntent.getActivity(contexto, 0,
				intent, android.content.Intent.FLAG_ACTIVITY_NEW_TASK);

		notification.setLatestEventInfo(contexto, titulo, descripcion,
				contIntent);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		notificationManager.notify(1, notification);
	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		Log.d("GCMIntentService_PasaNoticias", "REGISTRATION: Desregistrado OK.");
	}

}
